from typing import List, Dict, Optional

from constants import APP_STATE


class Event:

    type: str
    created_at: int
    user_id: Optional[int] = None
    device_id: Optional[str] = None

    @staticmethod
    def get_events_list(events_list: List[dict]) -> List['Event']:
        events = [Event(type=d['t'], created_at=d['c'], user_id=d.get('u'), device_id=d.get('d'))
                  for d in events_list]
        return events

    @staticmethod
    def events_to_dict_list(events: List['Event']) -> List[dict]:
        result = []
        for e in events:
            result.append(e.to_dict())
        return result

    def __init__(self, type: str, created_at: int, user_id: Optional[int]=None, device_id: Optional[str]=None) -> None:
        self.type = type
        self.created_at = created_at
        self.user_id = user_id
        self.device_id = device_id

    def __repr__(self) -> str:
        d = self.to_dict()
        pairs = []
        for k, v in d.items():
            pairs.append('%s: %s' % (k, repr(v)))

        return '{%s}' % ', '.join(pairs)  # ->  "{'t': 'c', 'c': 2, 'u': 1, 'd': '2'}"

    @property
    def is_device_event(self) -> bool:
        return self.type == 'c' or self.type == 'd'

    @property
    def is_connect(self) -> bool:
        return self.type == 'c'

    @property
    def is_disconnect(self) -> bool:
        return self.type == 'd'

    @property
    def is_pause(self) -> bool:
        return self.type == 'p'

    @property
    def is_unpause(self) -> bool:
        return self.type == 'u'

    @property
    def is_start(self) -> bool:
        return self.type == 's'

    @property
    def is_end(self) -> bool:
        return self.type == 'e'

    def to_dict(self) -> dict:
        d = {}
        d['t'] = self.type
        d['c'] = self.created_at
        if self.user_id is not None:
            d['u'] = self.user_id
        if self.device_id is not None:
            d['d'] = self.device_id
        return d


class Lesson:

    connected_devices: Dict[str, int]
    tracked_time: int = 0
    last_both_connected: Optional[int] = None
    last_active: Optional[int] = None
    on_pause: bool = False
    state: Optional[int] = None
    state_time: Optional[int] = None

    def __init__(self):
        self.connected_devices = {}
