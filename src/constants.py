import typing


class APP_STATE(typing.NamedTuple):
    IDLE: int = 0
    IN_PROGRESS: int = 1
    PAUSED: int = 2
