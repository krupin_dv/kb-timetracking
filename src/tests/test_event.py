from unittest import TestCase

from models import Event



class EventTestCase(TestCase):

    def test_get_events_list(self):
        events_list = [
            {'t': 's', 'c': 1},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 4, 'u': 1, 'd': '1'},
            {'t': 'e', 'c': 3},
        ]

        events = Event.get_events_list(events_list)

        e = events[0]
        self.assertEqual(e.type, 's')
        self.assertEqual(e.created_at, 1)
        self.assertEqual(e.user_id, None)
        self.assertEqual(e.device_id, None)

        e = events[1]
        self.assertEqual(e.type, 'c')
        self.assertEqual(e.created_at, 2)
        self.assertEqual(e.user_id, 1)
        self.assertEqual(e.device_id, '1')

        events_list2 = Event.events_to_dict_list(events)
        self.assertEqual(events_list, events_list2)
