from pprint import pprint
from unittest import TestCase

from models import Event
from flatten import find_device_streams, flatten_device_stream, flatten_event_stream



class FindDeviceStreamTestCase(TestCase):

    events_list = [
        {'t': 's', 'c': 0},
        {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
        {'t': 'p', 'c': 0},
        {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
        {'t': 'd', 'c': 3, 'u': 1, 'd': '1'},
        {'t': 'u', 'c': 3},
        {'t': 'e', 'c': 4},
    ]

    def test_should_find_device_streams(self):
        events = Event.get_events_list(self.events_list)
        device_streams, other_events = find_device_streams(events)

        self.assertEqual(device_streams['1'][0].to_dict(), {'t': 'c', 'c': 1, 'u': 1, 'd': '1'})
        self.assertEqual(device_streams['1'][1].to_dict(), {'t': 'd', 'c': 3, 'u': 1, 'd': '1'})
        self.assertEqual(device_streams['2'][0].to_dict(), {'t': 'c', 'c': 1, 'u': 2, 'd': '2'})

    def test_should_find_other_events(self):
        events = Event.get_events_list(self.events_list)
        device_streams, other_events = find_device_streams(events)
        self.assertEqual(other_events[0].to_dict(),  {'t': 's', 'c': 0})
        self.assertEqual(other_events[1].to_dict(),  {'t': 'p', 'c': 0})
        self.assertEqual(other_events[2].to_dict(),  {'t': 'u', 'c': 3})
        self.assertEqual(other_events[3].to_dict(),  {'t': 'e', 'c': 4})


class FlattenDeviceStreamTestCase(TestCase):

    def test_on_empty_list(self):
        device_streams, other_events = find_device_streams([])
        self.assertEqual(device_streams, {})
        self.assertEqual(other_events, [])

    def test_should_flatten_connect_events(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 3, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 4)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0].to_dict(), {'t': 'c', 'c': 0, 'u': 1, 'd': '1'})

    def test_should_flatten_connect_events_with_disconnect_siblings(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 3, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 4)
        self.assertEqual(len(res), 2)
        self.assertEqual(res[0].to_dict(), {'t': 'c', 'c': 0, 'u': 1, 'd': '1'})
        self.assertEqual(res[1].to_dict(), {'t': 'd', 'c': 3, 'u': 1, 'd': '1'})

    def test_should_add_trailing_disconnect_event(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 3, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 10)
        self.assertEqual(len(res), 2)
        self.assertEqual(res[0].to_dict(), {'t': 'c', 'c': 0, 'u': 1, 'd': '1'})
        self.assertEqual(res[1].to_dict(), {'t': 'd', 'c': 5, 'u': 1, 'd': '1'})

    def test_should_not_add_trailing_disconnect_event_in_advance(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 3, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 5)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0].to_dict(), {'t': 'c', 'c': 3, 'u': 1, 'd': '1'})

    def test_should_add_disconnect_events_on_timeout(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 5, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 6)
        events_list_res = Event.events_to_dict_list(res)
        self.assertEqual(events_list_res, [
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 5, 'u': 1, 'd': '1'},
        ])

    def test_should_flatten_disconnect_events(self):
        events = Event.get_events_list([
            {'t': 'd', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 0, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 10)
        events_list_res = Event.events_to_dict_list(res)
        self.assertEqual(events_list_res, [
            {'t': 'd', 'c': 0, 'u': 1, 'd': '1'},
        ])

    def test_should_flatten_injected_disconnect_events(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 6, 'u': 1, 'd': '1'},
        ])

        res = flatten_device_stream(events, 4, 10)
        events_list_res = Event.events_to_dict_list(res)
        self.assertEqual(events_list_res, [
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 2, 'u': 1, 'd': '1'},
        ])


class FlattenEventStreamTestCase(TestCase):

    def test_should_sort_import(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 1},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 4, 'u': 1, 'd': '1'},
            {'t': 'e', 'c': 3},
        ])

        res = flatten_event_stream(events, 4, 4)
        res_event_list = Event.events_to_dict_list(res)
        self.assertEqual(res_event_list, [
            {'t': 's', 'c': 1},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'e', 'c': 3},
        ])

    def test_should_skip_duplicate_pause_events(self):
        events = Event.get_events_list([
            {'t': 'p', 'c': 1},
            {'t': 'p', 'c': 2},
            {'t': 'u', 'c': 3},
            {'t': 'u', 'c': 4},
        ])

        res = flatten_event_stream(events, 4, 10)
        res_event_list = Event.events_to_dict_list(res)
        self.assertEqual(res_event_list, [
            {'t': 'p', 'c': 1},
            {'t': 'u', 'c': 3},
        ])

    def test_should_stop_on_event_end(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 1},
            {'t': 'p', 'c': 2},
            {'t': 'e', 'c': 3},
            {'t': 'u', 'c': 4},
        ])

        res = flatten_event_stream(events, 4, 10)
        res_event_list = Event.events_to_dict_list(res)
        self.assertEqual(res_event_list, [
            {'t': 's', 'c': 1},
            {'t': 'p', 'c': 2},
            {'t': 'e', 'c': 3},
        ])

    def test_should_flatten_app_stream(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'p', 'c': 1},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 3, 'u': 2, 'd': '2'},
            {'t': 'c', 'c': 6, 'u': 1, 'd': '1'},
            {'t': 'u', 'c': 7},
        ])

        res = flatten_event_stream(events, 4, 10)
        res_event_list = Event.events_to_dict_list(res)
        self.assertEqual(res_event_list, [
            {'t': 's', 'c': 0},
            {'t': 'p', 'c': 1},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 3, 'u': 2, 'd': '2'},
            {'t': 'd', 'c': 4, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 5, 'u': 2, 'd': '2'},
            {'t': 'c', 'c': 6, 'u': 1, 'd': '1'},
            {'t': 'u', 'c': 7},
            {'t': 'd', 'c': 8, 'u': 1, 'd': '1'},
        ])
