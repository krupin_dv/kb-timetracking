from pprint import pprint
from typing import List, Optional
from unittest import TestCase

from constants import APP_STATE
from models import Event
from reduce import reduce


def reduce_tracked_time(events: List[Event]) -> int:
    return reduce(events).tracked_time


def reduce_last_active(events: List[Event]) -> Optional[int]:
    return reduce(events).last_active


def reduce_state_time(events: List[Event]) -> Optional[int]:
    return reduce(events).state_time


def reduce_state(events: List[Event]) -> int:
    return reduce(events).state


class ReduceTracketTimeTestCase(TestCase):

    def test_should_be_0_with_no_users(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 0)

    def test_should_be_0_with_one_user(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '2'},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 0)

    def test_should_track_interval_with_both_users_connected(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'd', 'c': 3, 'u': 2, 'd': '2'},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 2)

    def test_should_track_interval_with_end_event(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'e', 'c': 3},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 2)

    def test_should_handle_pause_with_both_users_connected(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'p', 'c': 3},
            {'t': 'u', 'c': 4},
            {'t': 'e', 'c': 6},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 4)

    def test_should_track_connecting_while_pausing(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'p', 'c': 1},
            {'t': 'c', 'c': 3, 'u': 2, 'd': '2'},
            {'t': 'u', 'c': 4},
            {'t': 'e', 'c': 6},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 2)

    def test_should_track_uppausing_while_disconnected_user(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'p', 'c': 1},
            {'t': 'c', 'c': 3, 'u': 2, 'd': '2'},
            {'t': 'd', 'c': 4, 'u': 2, 'd': '2'},
            {'t': 'u', 'c': 5},
            {'t': 'e', 'c': 6},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 0)

    def test_should_end_with_end_event(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 0, 'u': 2, 'd': '2'},
            {'t': 'e', 'c': 3},
            {'t': 'c', 'c': 4, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 4, 'u': 2, 'd': '2'},
            {'t': 'd', 'c': 5, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 5, 'u': 2, 'd': '2'},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 3)

    def test_additional_devices_shouldnt_change_tracked_time(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 0, 'u': 2, 'd': '2'},
            {'t': 'c', 'c': 10, 'u': 1, 'd': '3'},
            {'t': 'd', 'c': 50, 'u': 1, 'd': '3'},
            {'t': 'd', 'c': 100, 'u': 1, 'd': '1'},
            {'t': 'd', 'c': 100, 'u': 2, 'd': '2'},
        ])
        res = reduce_tracked_time(events)
        self.assertEqual(res, 100)


class LastActiveTimeTestCase(TestCase):

    def test_should_be_none_with_one_user(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '2'},
        ])
        res = reduce_last_active(events)
        self.assertEqual(res, None)

    def test_should_be_last_connected_time_with_two_users(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
        ])
        res = reduce_last_active(events)
        self.assertEqual(res, 1)

    def test_should_be_unpaused_time_with_two_users(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'p', 'c': 2},
            {'t': 'u', 'c': 3},
        ])
        res = reduce_last_active(events)
        self.assertEqual(res, 3)

    def test_should_keep_connected_time_with_one_user_unpaused(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'p', 'c': 2},
            {'t': 'd', 'c': 3, 'u': 1, 'd': '1'},
            {'t': 'u', 'c': 4},
        ])
        res = reduce_last_active(events)
        self.assertEqual(res, 1)


class StateTimeTestCase(TestCase):

    def test_should_be_none_with_empty_events(self):
        self.assertIsNone(reduce_state_time([]))

    def test_should_be_last_event_timestamp(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'p', 'c': 1},
        ])
        res = reduce_state_time(events)
        self.assertEqual(res, 1)


class AppStateTestCase(TestCase):

    def test_should_be_idle_at_start(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IDLE)

    def test_should_be_paused(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'p', 'c': 1},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.PAUSED)

    def test_should_be_unpaused(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'p', 'c': 1},
            {'t': 'u', 'c': 2},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IDLE)

    def test_should_be_idle_with_one_user(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IDLE)

    def test_should_be_idle_with_one_user_from_two_devices(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 1, 'd': '2'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IDLE)

    def test_should_be_in_progress_with_two_users(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 2, 'd': '2'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IN_PROGRESS)

    def test_should_be_in_progress_with_two_users_frow_two_devices(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 2, 'u': 2, 'd': '2'},
            {'t': 'c', 'c': 3, 'u': 1, 'd': '3'},
            {'t': 'c', 'c': 4, 'u': 2, 'd': '4'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IN_PROGRESS)

    def test_should_be_paused_with_two_users(self):
        events = Event.get_events_list([
            {'t': 's', 'c': 0},
            {'t': 'c', 'c': 1, 'u': 1, 'd': '1'},
            {'t': 's', 'c': 2},
            {'t': 'c', 'c': 3, 'u': 2, 'd': '2'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IN_PROGRESS)

    def test_should_be_idle_when_user_disconnects(self):
        events = Event.get_events_list([
            {'t': 'c', 'c': 0, 'u': 1, 'd': '1'},
            {'t': 'c', 'c': 1, 'u': 2, 'd': '2'},
            {'t': 'd', 'c': 2, 'u': 2, 'd': '2'},
        ])
        res = reduce_state(events)
        self.assertEqual(res, APP_STATE.IDLE)
