from typing import Dict, List
from collections import namedtuple

from constants import APP_STATE
from models import Event, Lesson


def connected(connected_devices: Dict[str, int]) -> bool:
    first_user_id = None

    for device_id, user_id in connected_devices.items():
        if first_user_id is None:
            first_user_id = user_id
        elif not first_user_id == user_id:
            return True

    return False


def reduce(events: List[Event]) -> Lesson:
    l = Lesson()

    for e in events:
        if e.is_connect:
            prev_connected = connected(l.connected_devices)
            l.connected_devices[e.device_id] = e.user_id
            if not prev_connected and connected(l.connected_devices) and not l.on_pause:
                l.last_both_connected = e.created_at
                l.last_active = e.created_at
        elif e.is_disconnect:
            prev_connected = connected(l.connected_devices)
            l.connected_devices.pop(e.device_id, None)  # remove from dict
            if prev_connected and l.last_both_connected is not None and not connected(l.connected_devices) and not l.on_pause:
                l.tracked_time += e.created_at - l.last_both_connected
                l.last_both_connected = None
        elif e.is_pause:
            if l.last_both_connected is not None and not l.on_pause:
                l.tracked_time += e.created_at - l.last_both_connected
                l.last_both_connected = None
            l.on_pause = True
        elif e.is_unpause:
            if connected(l.connected_devices):
                l.last_both_connected = e.created_at
                l.last_active = e.created_at
            l.on_pause = False
        elif e.is_end:
            if l.last_both_connected is not None and not l.on_pause:
                l.tracked_time += e.created_at - l.last_both_connected
                l.last_both_connected = None
            break

        l.state_time = e.created_at

    l.state = APP_STATE.IDLE
    if l.on_pause:
        l.state = APP_STATE.PAUSED
    elif connected(l.connected_devices):
        l.state = APP_STATE.IN_PROGRESS

    return l
