from typing import List

from models import Event


def flatten_event_stream(events: List[Event], ttl: int, current_time: int) -> List[Event]:
    events = sorted(events, key=lambda e: e.created_at)
    devices_streams, other_events = find_device_streams(events)
    result = other_events
    for device_id in devices_streams:
        result.extend(flatten_device_stream(devices_streams[device_id], ttl, current_time))

    return sorted(result, key=lambda e: e.created_at)


def find_device_streams(events: List[Event]) -> tuple:
    device_streams = {}  # {device_id: event}
    other_events = []
    paused = False

    for e in events:
        if e.is_device_event:
            if not e.device_id in device_streams:
                device_streams[e.device_id] = []
            device_streams[e.device_id].append(e)
        else:
            ignore = False
            if e.is_pause:
                ignore = paused
                paused = True
            elif e.is_unpause:
                ignore = not paused
                paused = False

            if not ignore:
                other_events.append(e)
            if e.is_end:
                break

    return device_streams, other_events


def flatten_device_stream(events: List[Event], ttl: int, current_time: int) -> List[Event]:
    if not events:
        return []

    last_event = events[0]
    result = [last_event]
    last_connected_time = None
    if last_event.is_connect:
        last_connected_time = last_event.created_at

    for i, e in enumerate(events):
        if i == 0:
            continue

        if e.is_connect:
            ignore = False
            if last_event.is_connect:
                if last_connected_time is not None:
                    if e.created_at - last_connected_time < ttl:
                        ignore = True
                    else:
                        result.append(Event('d', last_connected_time + ttl / 2, e.user_id, e.device_id))
            if not ignore:
                result.append(e)

            last_connected_time = e.created_at
        elif e.is_disconnect:
            if last_event.is_connect and last_connected_time is not None  and e.created_at - last_connected_time >= ttl:
                result.append(Event('d', last_connected_time + ttl / 2, e.user_id, e.device_id))
            elif not last_event.is_disconnect:
                result.append(e)

        last_event = result[len(result) - 1]

    if last_event.is_connect and last_connected_time is not None and current_time:
        if current_time - last_connected_time >= ttl:
            result.append(Event('d', last_connected_time + ttl / 2, last_event.user_id, last_event.device_id))

    return result
